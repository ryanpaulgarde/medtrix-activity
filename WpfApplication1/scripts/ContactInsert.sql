use Activity
go

IF EXISTS (SELECT * FROM sys.procedures WHERE name = 'ContactInsert')
	DROP PROCEDURE [ContactInsert]
GO

CREATE PROCEDURE [ContactInsert]
(
	@Name VARCHAR(255),
	@Address VARCHAR(255),
	@ContactNumber VARCHAR(255),
	@ContactNumberType VARCHAR(255),
	@ContactEmailAddress VARCHAR(255),
	@ContactEmailAddressType VARCHAR(255),
	@ContactNote VARCHAR(255),
	@Type VARCHAR(255),
	@ID VARCHAR(255) = NULL
)
WITH ENCRYPTION 
AS
BEGIN TRANSACTION 
SET NOCOUNT OFF
BEGIN TRY
	
	BEGIN
	IF(@Type = 'Edit')
		UPDATE Contacts SET Name = @Name, Address = @Address
		WHERE ContactID = @ID
	ELSE
		INSERT INTO Contacts(Name, Address, UserID, IsActive)   
		VALUES(@Name, @Address, 1, 1)
	END

	DECLARE @ContactID INT;
	SET @ContactID = SCOPE_IDENTITY()--(SELECT ContactID AS LastID FROM Contacts WHERE ContactID = @@Identity)
	
	BEGIN
	IF((@ContactNumber IS NOT NULL OR @ContactNumber != '') AND @ContactID IS NOT NULL)
		INSERT INTO ContactNumbers(Name, Type, ContactID)   
		VALUES(@ContactNumber, @ContactNumberType, @ContactID);
	
	IF(@ContactEmailAddress IS NOT NULL AND @ContactID IS NOT NULL)
		INSERT INTO ContactEmailAddresses(Name, Type, ContactID)   
		VALUES(@ContactEmailAddress, @ContactEmailAddressType, @ContactID)

	IF(@ContactNote IS NOT NULL AND @ContactID IS NOT NULL)
		INSERT INTO ContactNotes(Name, ContactID)   
		VALUES(@ContactNote, @ContactID)
	END


COMMIT TRANSACTION
END TRY
BEGIN CATCH
	ROLLBACK TRANSACTION
	
        DECLARE @ErrorNum INT = ERROR_NUMBER();  
        DECLARE @ErrorLine INT = ERROR_LINE();  
        DECLARE @ErrorMsg NVARCHAR(4000) = ERROR_MESSAGE();  
        DECLARE @ErrorSeverity INT = ERROR_SEVERITY();  
        DECLARE @ErrorState INT = ERROR_STATE();  
   RAISERROR(@ErrorMsg, @ErrorSeverity, @ErrorState);  
END CATCH
GO