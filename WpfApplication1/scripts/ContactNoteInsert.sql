use Activity
go

IF EXISTS (SELECT * FROM sys.procedures WHERE name = 'ContactNoteInsert')
	DROP PROCEDURE [ContactNoteInsert]
GO

CREATE PROCEDURE [ContactNoteInsert]
(
	@Name VARCHAR(255),
	@ContactID INT,
	@FormType VARCHAR(255),
	@ID VARCHAR(255) = NULL
)
WITH ENCRYPTION 
AS
BEGIN TRANSACTION 
SET NOCOUNT OFF
BEGIN TRY

	BEGIN
	IF(@FormType = 'Edit')
		UPDATE ContactNotes SET Name = @Name
		WHERE ContactNoteID = @ID
	ELSE
		INSERT INTO ContactNotes(Name, ContactID)   
		VALUES(@Name, @ContactID)
	END

COMMIT TRANSACTION
END TRY
BEGIN CATCH
	ROLLBACK TRANSACTION
	
        DECLARE @ErrorNum INT = ERROR_NUMBER();  
        DECLARE @ErrorLine INT = ERROR_LINE();  
        DECLARE @ErrorMsg NVARCHAR(4000) = ERROR_MESSAGE();  
        DECLARE @ErrorSeverity INT = ERROR_SEVERITY();  
        DECLARE @ErrorState INT = ERROR_STATE();  
   RAISERROR(@ErrorMsg, @ErrorSeverity, @ErrorState);  
END CATCH
GO