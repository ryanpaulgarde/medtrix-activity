use Activity
go

IF EXISTS (SELECT * FROM sys.procedures WHERE name = 'ContactEmailAddressDelete')
	DROP PROCEDURE [ContactEmailAddressDelete]
GO

CREATE PROCEDURE [ContactEmailAddressDelete]
(
	@ContactEmailAddressID int
)
WITH ENCRYPTION 
AS
BEGIN TRANSACTION 
SET NOCOUNT OFF
BEGIN TRY

	DELETE FROM ContactEmailAddresses WHERE ContactEmailAddressID = @ContactEmailAddressID;

COMMIT TRANSACTION
END TRY
BEGIN CATCH
	ROLLBACK TRANSACTION
	
        DECLARE @ErrorNum INT = ERROR_NUMBER();  
        DECLARE @ErrorLine INT = ERROR_LINE();  
        DECLARE @ErrorMsg NVARCHAR(4000) = ERROR_MESSAGE();  
        DECLARE @ErrorSeverity INT = ERROR_SEVERITY();  
        DECLARE @ErrorState INT = ERROR_STATE();  
   RAISERROR(@ErrorMsg, @ErrorSeverity, @ErrorState);  
END CATCH
GO