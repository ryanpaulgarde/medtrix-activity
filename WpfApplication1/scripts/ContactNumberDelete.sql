use Activity
go

IF EXISTS (SELECT * FROM sys.procedures WHERE name = 'ContactNumberDelete')
	DROP PROCEDURE [ContactNumberDelete]
GO

CREATE PROCEDURE [ContactNumberDelete]
(
	@ContactNumberID int
)
WITH ENCRYPTION 
AS
BEGIN TRANSACTION 
SET NOCOUNT OFF
BEGIN TRY

	DELETE FROM ContactNumbers WHERE ContactNumberID = @ContactNumberID;

COMMIT TRANSACTION
END TRY
BEGIN CATCH
	ROLLBACK TRANSACTION
	
        DECLARE @ErrorNum INT = ERROR_NUMBER();  
        DECLARE @ErrorLine INT = ERROR_LINE();  
        DECLARE @ErrorMsg NVARCHAR(4000) = ERROR_MESSAGE();  
        DECLARE @ErrorSeverity INT = ERROR_SEVERITY();  
        DECLARE @ErrorState INT = ERROR_STATE();  
   RAISERROR(@ErrorMsg, @ErrorSeverity, @ErrorState);  
END CATCH
GO