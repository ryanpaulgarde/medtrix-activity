use Activity
go

--DROP TABLE ContactEmailAddresses;
--DROP TABLE ContactNotes;
--DROP TABLE ContactNumbers;
--DROP TABLE Contacts;
--DROP TABLE Users;


CREATE TABLE Users (
	[ID] INT IDENTITY(1,1),
	[Name] varchar(255),
	CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED ([ID] ASC)
);

CREATE TABLE Contacts (
	[ID] INT IDENTITY(1,1),
	[Name] varchar(255),
	[Address] varchar(255),
	[UserID] INT NOT NULL,
	CONSTRAINT [PK_Contacts] PRIMARY KEY CLUSTERED ([ID] ASC),
	CONSTRAINT [fk_Contacts_Users] FOREIGN KEY ([UserID]) REFERENCES [Users] ([ID]) ON DELETE CASCADE,
);

CREATE TABLE ContactNumbers (
	[ID] INT IDENTITY(1,1) PRIMARY KEY,
	[Name] varchar(255),
	[Type] varchar(255),
	[ContactID] INT NOT NULL
	CONSTRAINT [fk_ContactNumbers_Contacts] FOREIGN KEY ([ContactID]) REFERENCES [Contacts] ([ID]) ON DELETE CASCADE,
);

CREATE TABLE ContactEmailAddresses(
	[ID] INT IDENTITY(1,1) PRIMARY KEY,
	[Name] varchar(255),
	[Type] varchar(255),
	[ContactID] INT NOT NULL
	CONSTRAINT [fk_ContactEmailAddresses_Contacts] FOREIGN KEY ([ContactID]) REFERENCES [Contacts] ([ID]) ON DELETE CASCADE,
);

CREATE TABLE ContactNotes(
	[ID] INT IDENTITY(1,1) PRIMARY KEY,
	[Name] varchar(255),
	[ContactID] INT NOT NULL
	CONSTRAINT [fk_ContactNotes_Contacts] FOREIGN KEY ([ContactID]) REFERENCES [Contacts] ([ID]) ON DELETE CASCADE,
);

