use Activity
go

IF EXISTS (SELECT * FROM sys.procedures WHERE name = 'ContactEmailAddressInsert')
	DROP PROCEDURE [ContactEmailAddressInsert]
GO

CREATE PROCEDURE [ContactEmailAddressInsert]
(
	@Name VARCHAR(255),
	@Type VARCHAR(255),
	@ContactID INT,
	@FormType VARCHAR(255),
	@ID VARCHAR(255) = NULL
)
WITH ENCRYPTION 
AS
BEGIN TRANSACTION 
SET NOCOUNT OFF
BEGIN TRY

	BEGIN
	IF(@FormType = 'Edit')
		UPDATE ContactEmailAddresses SET Name = @Name, Type = @Type
		WHERE ContactEmailAddressID = @ID
	ELSE
		INSERT INTO ContactEmailAddresses(Name, Type, ContactID)   
		VALUES(@Name,@Type, @ContactID)
	END

COMMIT TRANSACTION
END TRY
BEGIN CATCH
	ROLLBACK TRANSACTION
	
        DECLARE @ErrorNum INT = ERROR_NUMBER();  
        DECLARE @ErrorLine INT = ERROR_LINE();  
        DECLARE @ErrorMsg NVARCHAR(4000) = ERROR_MESSAGE();  
        DECLARE @ErrorSeverity INT = ERROR_SEVERITY();  
        DECLARE @ErrorState INT = ERROR_STATE();  
   RAISERROR(@ErrorMsg, @ErrorSeverity, @ErrorState);  
END CATCH
GO