use Activity
go

IF EXISTS (SELECT * FROM sys.procedures WHERE name = 'ContactNumberInsert')
	DROP PROCEDURE [ContactNumberInsert]
GO

CREATE PROCEDURE [ContactNumberInsert]
(
	@Name VARCHAR(255),
	@Type VARCHAR(255),
	@ContactID INT
)
WITH ENCRYPTION 
AS
BEGIN TRANSACTION 
SET NOCOUNT OFF
BEGIN TRY

	INSERT INTO ContactNumbers(Name, Type, ContactID)   
    VALUES(@Name,@Type, @ContactID)

COMMIT TRANSACTION
END TRY
BEGIN CATCH
	ROLLBACK TRANSACTION
	
        DECLARE @ErrorNum INT = ERROR_NUMBER();  
        DECLARE @ErrorLine INT = ERROR_LINE();  
        DECLARE @ErrorMsg NVARCHAR(4000) = ERROR_MESSAGE();  
        DECLARE @ErrorSeverity INT = ERROR_SEVERITY();  
        DECLARE @ErrorState INT = ERROR_STATE();  
   RAISERROR(@ErrorMsg, @ErrorSeverity, @ErrorState);  
END CATCH
GO