use Activity
go

DROP VIEW IF EXISTS ContactList
GO

CREATE VIEW ContactList AS
	SELECT * FROM Contacts
GO