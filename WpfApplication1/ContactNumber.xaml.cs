﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Activity
{
    /// <summary>
    /// Interaction logic for ContactNumber.xaml
    /// </summary>
    public partial class ContactNumber : Window
    {
        int contactId;
        public ContactNumber(int contactId)
        {
            InitializeComponent();

            this.contactId = contactId;

            string connectionString = null;
            string sql = null;

            SqlConnection connection;
            SqlCommand command;

            connectionString = "Data Source=ServerName; Server=localhost; Database=Activity; User ID=sa; Password=123456; Integrated Security=SSPI";
            sql = $"select * from ContactNumbers where ContactID='{this.contactId}'";
            connection = new SqlConnection(connectionString);
            try
            {
                command = new SqlCommand(sql, connection);

                DataTable dtContactNumbers = new DataTable();
                SqlDataAdapter adapter = new SqlDataAdapter(command);

                connection.Open();


                adapter.Fill(dtContactNumbers);
                dgContactNumbers.ItemsSource = dtContactNumbers.DefaultView;

                command.Dispose();
                connection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void AddContactNumber(object sender, RoutedEventArgs e)
        {
            ContactNumberCreate cncForm = new ContactNumberCreate(this.contactId);
            cncForm.ShowDialog();
        }
    }
}
