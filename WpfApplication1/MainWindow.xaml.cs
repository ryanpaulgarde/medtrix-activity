﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data.SqlClient;
using System.Data;
using Activity;

namespace WpfApplication1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            string connectionString = null;
            string sql = null;

            SqlConnection connection;
            SqlCommand command;

            connectionString = "Data Source=ServerName; Server=localhost; Database=Activity; User ID=sa; Password=123456; Integrated Security=SSPI";
            sql = "select * from Contacts";
            connection = new SqlConnection(connectionString);
            try
            {
                command = new SqlCommand(sql, connection);

                DataTable dtContactNumbers = new DataTable();
                SqlDataAdapter adapter = new SqlDataAdapter(command);

                connection.Open();
                

                adapter.Fill(dtContactNumbers);
                dgContacts.ItemsSource = dtContactNumbers.DefaultView;

                command.Dispose();
                connection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

  
        private void ButtonClickEvent(object sender, RoutedEventArgs e)
        {
            try
            {
                if(sender == createContact)
                {
                    Contact contactForm = new Contact();
                    contactForm.ShowDialog();
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void ShowContactNumbers(object sender, RoutedEventArgs e)
        {
            DataRowView dr = dgContacts.SelectedItem as DataRowView;
            int contactid = Convert.ToInt32(dr["ID"].ToString());
            ContactNumber contactNumberForm = new ContactNumber(contactid);
            contactNumberForm.ShowDialog();
        }

        private void ShowContactEmailAddresses(object sender, RoutedEventArgs e)
        {
            DataRowView dr = dgContacts.SelectedItem as DataRowView;
            int contactid = Convert.ToInt32(dr["ID"].ToString());
            MessageBox.Show(contactid.ToString());
        }

        private void ShowContactNotes(object sender, RoutedEventArgs e)
        {
            DataRowView dr = dgContacts.SelectedItem as DataRowView;
            int contactid = Convert.ToInt32(dr["ID"].ToString());
            MessageBox.Show(contactid.ToString());
        }
    }
}
