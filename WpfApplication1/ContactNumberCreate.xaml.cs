﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Activity
{
    public partial class ContactNumberCreate : Window
    {
        int contactId;
        public ContactNumberCreate(int contactId)
        {
            InitializeComponent();

            this.contactId = contactId;
            
        }

        private void CreateContactNumber(object sender, RoutedEventArgs e)
        {
            string connectionString = null;
            string sql = null;

            SqlConnection connection;
            SqlCommand command;

            connectionString = "Data Source=ServerName; Server=localhost; Database=Activity; User ID=sa; Password=123456; Integrated Security=SSPI";

            connection = new SqlConnection(connectionString);
            try
            {
                connection.Open();

                command = new SqlCommand(sql, connection);

                DataTable dtContactNumbers = new DataTable();
                SqlDataAdapter adapter = new SqlDataAdapter(command);

                command.CommandType = System.Data.CommandType.StoredProcedure;
                command.CommandText = "ContactNumberInsert";
                command.Parameters.AddWithValue("@Name", txtName.Text);
                command.Parameters.AddWithValue("@Type", txtType.Text);
                command.Parameters.AddWithValue("@ContactID", this.contactId);

                adapter.Fill(dtContactNumbers);

                command.Dispose();
                connection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
